<!DOCTYPE html>
<html>
<head>
    <title>Sign Up</title>
</head>
<body>

    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name : </label> <br><br>
        <input type="text" name="first_name" required> <br><br>

        <label>Last name : </label> <br><br>
        <input type="text" name="last_name" required> <br><br>

        <label>Gender</label> <br><br>
        <input type="radio" name="gender" value="male">Male <br>
        <input type="radio" name="gender" value="female">Female <br><br>

        <label>Nationality</label> <br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select> <br><br>

        <label>Language Spoken</label> <br><br>
        <input type="checkbox" name="language" value="Indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="English">English <br>
        <input type="checkbox" name="language" value="Other">Other <br><br>

        <label>Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up"> <br> <br>

    </form>
    
</body>
</html>