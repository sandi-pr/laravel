<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CharController extends Controller
{
    public function index()
    {
        $char = DB::table('characters')->get();
        return view('char.index', compact('char'));
    }

    public function create() {
        return view('char.create');
    }

    public function store(Request $request) {
        $request->validate(
            [
                'nama' => 'required',
                'description' => 'required',
            ],
            [
                'nama.required' => 'Inputan Nama harus diisi',
                'description.required' => 'Inputan Deskripsi harus diisi'
            ]
        );

        DB::table('characters')->insert (
            [
                'nama' => $request['nama'],
                'description' => $request['description']
            ]
        );
        return redirect('/char');
    }

    public function show($id)
    {
        $char = DB::table('characters')->where('id', $id)->first();
        return view('char.show', compact('char'));
    }

    public function edit($id)
    {
        $char = DB::table('characters')->where('id', $id)->first();
        return view('char.update', compact('char'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'description' => 'required',
        ]);

        DB::table('characters')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'description' => $request['description']
            ]);
        return redirect('/char');
    }

    public function destroy($id)
    {
        DB::table('characters')->where('id', $id)->delete();
        return redirect('/char');
    }
}