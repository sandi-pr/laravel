<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('char_list', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('anime_id');
            $table->foreign('anime_id')->references('id')->on('anime')->onDelete('cascade');
            $table->unsignedBigInteger('char_id');
            $table->foreign('char_id')->references('id')->on('characters')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('char_list');
    }
}
