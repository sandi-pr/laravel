@extends('layout.master')
@section('judul')
    List Karakter
@endsection
@section('content')
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($char as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->description}}</td>
                    <td>
                        <form action="/char/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/char/{{$value->id}}" class="btn btn-info">Detail</a>
                            <a href="/char/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <input type="submit" class="btn btn-danger my-1" value="Hapus">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>Data Kosong</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
    <a href="/char/create" class="btn btn-primary">Tambah</a>
@endsection