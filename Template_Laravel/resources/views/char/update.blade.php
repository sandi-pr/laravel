@extends('layout.master')
@section('judul')
    Edit Karakter
@endsection
@section('content')
    <div>
        <form action="/char/{{$char->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$char->nama}}" id="nama" placeholder="Masukkan Nama Karakter">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Deskripsi</label>
                <textarea type="text" class="form-control" name="description" id="description" placeholder="Masukkan Deskripsi">{{$char->description}}</textarea>
                @error('description')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
    @endsection