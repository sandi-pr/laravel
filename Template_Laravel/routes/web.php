<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/table', function () {
    return view('content/table');
});
Route::get('/data-tables', function () {
    return view('content/data-tables');
});

//CRUD Characters
Route::get('/char', 'CharController@index');
Route::get('/char/create', 'CharController@create');
Route::post('/char', 'CharController@store');
Route::get('/char/{char_id}', 'CharController@show');
Route::get('/char/{char_id}/edit', 'CharController@edit');
Route::put('/char/{char_id}', 'CharController@update');
Route::delete('/char/{char_id}', 'CharController@destroy');

